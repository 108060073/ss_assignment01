# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    The painter has servral functions, click the button on the tool bar to use:
    
    -------------------------------------------------------------------------
    Brush: Draw on the canvas.
    -------------------------------------------------------------------------
    Eraser: Use the eraser to clear.
    -------------------------------------------------------------------------
    Text: To type texts, click anywhere on the canvas then use keyboard to
    enter keys. Text function supports word-wrap and backspace.
    -------------------------------------------------------------------------
    Line: Draw a straight line on the canvas. Your mousedown position will be
    the beginning of the line, then move the cursor anywhere on the canvas, 
    mouseup to end the line.
    -------------------------------------------------------------------------
    Rectangle & Triangle & Circle: 
    Draw the corresponding shape on the canvas. You can use the mouse to con-
    trol the size of them. Press the 'Fill' button to set whether to fill th-
    ese shapes.
    -------------------------------------------------------------------------
    Undo: Press to undo the action.
    -------------------------------------------------------------------------
    Redo: Press to redo the action.
    -------------------------------------------------------------------------
    Fill: Press to change the fill mode of drawing shapes.
    -------------------------------------------------------------------------
    Download: Press to download the canvas image to localhost.
    -------------------------------------------------------------------------
    Refresh: Reset the canvas, this action could be undo.
    -------------------------------------------------------------------------
    SizeSlider(left): Toggle the handler to set the brush size. (Also affect 
    shape border)
    -------------------------------------------------------------------------
    Upload: Choose an image to cover on the canvas from position (0,0).
    -------------------------------------------------------------------------
    Font & FontSize: Font settings.

### Function description

    SizeSlider(left): Toggle the handler to set the brush alpha channel. (Also 
    affect shape border)

### Gitlab page link

    https://108060073.gitlab.io/ss_assignment01/

### Others (Optional)

    Not a very easy assignment @-@

<style>
table th{
    width: 100%;
}
</style>